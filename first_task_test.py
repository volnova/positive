"""First task unit tests"""
from first_task import return_seconds


class TestFirstTask:
    """Class for testing return_seconds function"""

    def test_value_in_seconds(self):
        assert return_seconds('30s') == 30.0

    def test_value_in_seconds_without_digits(self):
        assert return_seconds('s') == 1.0

    def test_value_without_units(self):
        assert return_seconds('30') == 30.0

    def test_value_without_units_float(self):
        assert return_seconds('30.65') == 30.65

    def test_value_in_minutes(self):
        assert return_seconds('30m') == 1800.0

    def test_value_in_minutes_without_digits(self):
        assert return_seconds('m') == 60.0

    def test_value_in_hours(self):
        assert return_seconds('10h') == 36000.0

    def test_value_in_hours_without_digits(self):
        assert return_seconds('h') == 3600.0

    def test_value_in_days(self):
        assert return_seconds('5d') == 432000.0

    def test_value_in_days_without_digits(self):
        assert return_seconds('d') == 86400.0

    def test_value_with_incorrect_unit(self):
        assert return_seconds('10k') == 'Please input a value in a correct format!'

    def test_value_with_incorrect_format_with_digits(self):
        assert return_seconds('10d10s') == 'Please input a value in a correct format!'

    def test_value_with_incorrect_format_without_digits(self):
        assert return_seconds('df') == 'Please input a value in a correct format!'

"""Second task solution"""


def file_names_generator(*args):
    """Return endless generator of file names"""
    while True:
        for file in args:
            yield file


def file_content_generator(filename):
    """Return generator of file content line by line"""
    with open(filename) as file:
        for line in file:
            yield line.strip()


def generator_from_files(*args):
    """
    Endless print content of given files in a multiplexed
    file-by-file line-by-line order
    """
    data_dict = {}

    def update_data_dict(file_name):
        gen = file_content_generator(file_name)
        data_dict[file_name] = [gen, next(gen)]

    for filename in args:
        update_data_dict(filename)
    for filename in file_names_generator(*args):
        print(data_dict[filename][1])
        try:
            data_dict[filename][1] = next(data_dict[filename][0])
        except StopIteration:
            update_data_dict(filename)


generator_from_files('one.txt', 'two.txt', 'three.txt')

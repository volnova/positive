"""Third task solution"""


def merge(*args):
    """Merge given generators and return sorted data generator"""
    elements_list = []

    for iterable in args:
        while True:
            generator = iterable
            try:
                elements_list.append(next(generator))
            except StopIteration:
                break

    for element in sorted(elements_list):
        yield element

"""First task solution"""


def return_seconds(input_value):
    """
    Check input data and return a time delta
    in seconds if format is correct
    """
    units_data_dict = {
        's': 1,
        'm': 60,
        'h': 3600,
        'd': 86400
    }

    last_symbol = input_value[-1]
    is_unit_exists = last_symbol.isalpha()
    unit = last_symbol if is_unit_exists else 's'
    digit_value = input_value[:-1] if is_unit_exists and len(input_value) > 1 \
        else input_value if not input_value.isalpha() else 1

    try:
        return float(digit_value)*units_data_dict[unit]
    except (ValueError, KeyError):
        return 'Please input a value in a correct format!'

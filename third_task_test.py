"""Third task unit tests"""
from third_task import merge


class TestThirdTask:
    """Class for testing merge function"""

    def test_type_of_return_data(self):
        first_gen = (i for i in [1, 5, 9, 10, 13, 15, 19])
        second_gen = (i for i in [2])
        assert str(type(merge(first_gen, second_gen))) == "<class 'generator'>"

    def test_merge_two_generators(self):
        first_gen = (i for i in [1, 5, 9, 10, 13, 15, 19])
        second_gen = (i for i in [2, 3, 5])
        list_data = [i for i in merge(first_gen, second_gen)]
        assert list_data == [1, 2, 3, 5, 5, 9, 10, 13, 15, 19]

    def test_merge_three_generators(self):
        first_gen = (i for i in [1, 5, 9, 10, 13, 15, 19])
        second_gen = (i for i in [2, 3, 5])
        third_gen = (i for i in [4, 7, 7, 11])
        list_data = [i for i in merge(first_gen, second_gen, third_gen)]
        assert list_data == [1, 2, 3, 4, 5, 5, 7, 7, 9, 10, 11, 13, 15, 19]
